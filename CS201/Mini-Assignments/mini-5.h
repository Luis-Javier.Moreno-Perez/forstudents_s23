// jdh CS201 Spring 2023

#define MAXNAMELEN 32

typedef struct {
  char name[MAXNAMELEN];
  int id;
} Record;
