// jdh CS224 Spring 2023
// example of returning more than a single return value,
// using Object[]

public class Example {
  public static void main(String argv[]) {
    int limit = 5;
    Object rtnVal[] = returnTest(limit);
    int i1[] = (int[]) rtnVal[0];
    int i2[] = (int[]) rtnVal[1];
    System.out.print("first array is:");
    for (Integer i: i1) {
      System.out.print(" " + i);
    }
    System.out.println();
    System.out.print("second array is:");
    for (Integer i: i2) {
      System.out.print(" " + i);
    }
    System.out.println();
  }

  public static Object[] returnTest(int upper) {
    int arrOne[] = new int[upper];
    int arrTwo[] = new int[upper];

    for (int i=0; i<upper; ++i) {
      arrOne[i] = i;
      arrTwo[i] = 2*i;
    }

    Object rtnval[] = new Object[2];
    rtnval[0] = arrOne;
    rtnval[1] = arrTwo;
    return rtnval;
  }
}
